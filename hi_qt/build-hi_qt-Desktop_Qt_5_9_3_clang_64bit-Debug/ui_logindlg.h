/********************************************************************************
** Form generated from reading UI file 'logindlg.ui'
**
** Created by: Qt User Interface Compiler version 5.9.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDLG_H
#define UI_LOGINDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_LoginDlg
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *usrEdit;
    QLineEdit *pwdEdit;
    QPushButton *btnLogin;
    QPushButton *btnExit;

    void setupUi(QDialog *LoginDlg)
    {
        if (LoginDlg->objectName().isEmpty())
            LoginDlg->setObjectName(QStringLiteral("LoginDlg"));
        LoginDlg->resize(400, 300);
        label = new QLabel(LoginDlg);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(9, 44, 48, 16));
        label_2 = new QLabel(LoginDlg);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(9, 105, 36, 16));
        usrEdit = new QLineEdit(LoginDlg);
        usrEdit->setObjectName(QStringLiteral("usrEdit"));
        usrEdit->setGeometry(QRect(63, 44, 231, 20));
        pwdEdit = new QLineEdit(LoginDlg);
        pwdEdit->setObjectName(QStringLiteral("pwdEdit"));
        pwdEdit->setGeometry(QRect(63, 105, 221, 20));
        pwdEdit->setEchoMode(QLineEdit::Password);
        btnLogin = new QPushButton(LoginDlg);
        btnLogin->setObjectName(QStringLiteral("btnLogin"));
        btnLogin->setGeometry(QRect(60, 160, 75, 23));
        btnExit = new QPushButton(LoginDlg);
        btnExit->setObjectName(QStringLiteral("btnExit"));
        btnExit->setGeometry(QRect(180, 160, 75, 23));

        retranslateUi(LoginDlg);
        QObject::connect(btnExit, SIGNAL(clicked()), LoginDlg, SLOT(close()));

        QMetaObject::connectSlotsByName(LoginDlg);
    } // setupUi

    void retranslateUi(QDialog *LoginDlg)
    {
        LoginDlg->setWindowTitle(QApplication::translate("LoginDlg", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("LoginDlg", "\347\224\250\346\210\267\345\220\215\357\274\232", Q_NULLPTR));
        label_2->setText(QApplication::translate("LoginDlg", "\345\257\206\347\240\201\357\274\232", Q_NULLPTR));
        usrEdit->setPlaceholderText(QApplication::translate("LoginDlg", "\350\257\267\350\276\223\345\205\245\347\224\250\346\210\267\345\220\215", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        pwdEdit->setToolTip(QApplication::translate("LoginDlg", "\350\257\267\350\276\223\345\205\245\345\257\206\347\240\201", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pwdEdit->setStatusTip(QApplication::translate("LoginDlg", "\350\276\223\345\205\245\345\257\206\347\240\201", Q_NULLPTR));
#endif // QT_NO_STATUSTIP
        pwdEdit->setPlaceholderText(QApplication::translate("LoginDlg", "\350\257\267\350\276\223\345\205\245\345\257\206\347\240\201", Q_NULLPTR));
        btnLogin->setText(QApplication::translate("LoginDlg", "\347\231\273\345\275\225", Q_NULLPTR));
        btnExit->setText(QApplication::translate("LoginDlg", "\351\200\200\345\207\272", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class LoginDlg: public Ui_LoginDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDLG_H
