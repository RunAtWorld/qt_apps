/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_N;
    QAction *action_S;
    QAction *action_O;
    QAction *action_O_2;
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *lbLocalIp;
    QPushButton *btnGetIp;
    QPushButton *btnBiosId;
    QLabel *lblBiosId;
    QPushButton *btnPing;
    QTextEdit *txtPing;
    QMenuBar *menuBar;
    QMenu *menu_F;
    QMenu *menu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(510, 450);
        action_N = new QAction(MainWindow);
        action_N->setObjectName(QStringLiteral("action_N"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/myImages/myImages/\346\226\260\345\273\272.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_N->setIcon(icon);
        action_S = new QAction(MainWindow);
        action_S->setObjectName(QStringLiteral("action_S"));
        action_O = new QAction(MainWindow);
        action_O->setObjectName(QStringLiteral("action_O"));
        action_O_2 = new QAction(MainWindow);
        action_O_2->setObjectName(QStringLiteral("action_O_2"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/myImages/myImages/\346\237\245\350\257\242.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_O_2->setIcon(icon1);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 54, 12));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(60, 60, 75, 23));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(210, 60, 75, 23));
        lbLocalIp = new QLabel(centralWidget);
        lbLocalIp->setObjectName(QStringLiteral("lbLocalIp"));
        lbLocalIp->setGeometry(QRect(110, 300, 171, 31));
        btnGetIp = new QPushButton(centralWidget);
        btnGetIp->setObjectName(QStringLiteral("btnGetIp"));
        btnGetIp->setGeometry(QRect(20, 300, 75, 23));
        btnBiosId = new QPushButton(centralWidget);
        btnBiosId->setObjectName(QStringLiteral("btnBiosId"));
        btnBiosId->setGeometry(QRect(20, 330, 75, 23));
        lblBiosId = new QLabel(centralWidget);
        lblBiosId->setObjectName(QStringLiteral("lblBiosId"));
        lblBiosId->setGeometry(QRect(110, 330, 161, 31));
        btnPing = new QPushButton(centralWidget);
        btnPing->setObjectName(QStringLiteral("btnPing"));
        btnPing->setGeometry(QRect(240, 170, 75, 23));
        txtPing = new QTextEdit(centralWidget);
        txtPing->setObjectName(QStringLiteral("txtPing"));
        txtPing->setGeometry(QRect(240, 200, 261, 121));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 510, 23));
        menu_F = new QMenu(menuBar);
        menu_F->setObjectName(QStringLiteral("menu_F"));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu_F->menuAction());
        menuBar->addAction(menu->menuAction());
        menu_F->addAction(action_N);
        menu_F->addAction(action_S);
        menu->addAction(action_O_2);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        action_N->setText(QApplication::translate("MainWindow", "\346\226\260\345\273\272(&N)", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        action_N->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        action_S->setText(QApplication::translate("MainWindow", "\344\277\235\345\255\230(&S)", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        action_S->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        action_O->setText(QApplication::translate("MainWindow", "\346\211\223\345\274\200(&O)", Q_NULLPTR));
        action_O_2->setText(QApplication::translate("MainWindow", "\346\237\245\346\211\276(&F)", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        action_O_2->setToolTip(QApplication::translate("MainWindow", "\346\237\245\350\257\242\346\223\215\344\275\234", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        action_O_2->setShortcut(QApplication::translate("MainWindow", "Ctrl+F", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        label->setText(QApplication::translate("MainWindow", "hi,Qt", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", "\347\231\273\345\275\225", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "\346\226\207\346\234\254\347\274\226\350\276\221\345\231\250", Q_NULLPTR));
        lbLocalIp->setText(QApplication::translate("MainWindow", "local ip", Q_NULLPTR));
        btnGetIp->setText(QApplication::translate("MainWindow", "\346\234\254\345\234\260IP", Q_NULLPTR));
        btnBiosId->setText(QApplication::translate("MainWindow", "BiosId", Q_NULLPTR));
        lblBiosId->setText(QApplication::translate("MainWindow", "bios id", Q_NULLPTR));
        btnPing->setText(QApplication::translate("MainWindow", "ping", Q_NULLPTR));
        menu_F->setTitle(QApplication::translate("MainWindow", "\346\226\207\344\273\266(&F)", Q_NULLPTR));
        menu->setTitle(QApplication::translate("MainWindow", "\347\274\226\350\276\221", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
