#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logindlg.h"
//这个头文件不存在也没报错
#include "querydlg.h"
#include <QDialog>
#include <QCoreApplication>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QProcess>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label->setText("Helle");
    ui->pushButton->setText("登录");

    //创建新动作
    QAction *openAction = new QAction("&Open",this);
    //添加图标
    QIcon icon(":/myImages/myImages/打开.png");
    // 设置图标
    openAction->setIcon(icon);
    // 设置快捷键
    openAction->setShortcut(QKeySequence("Ctrl+O"));
    //设置代开动作
    ui->menu_F->addAction(openAction);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QDialog *dlg = new LoginDlg(this);
    dlg->show();
}

void MainWindow::on_pushButton_2_clicked()
{

}

void MainWindow::on_btnGetIp_clicked()
{
    ui->lbLocalIp->setText(this->read_ip_address());
}


QString MainWindow::read_ip_address()
{
    QString ip_address;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    qDebug()<<QHostAddress::LocalHost;
    for (int i = 0; i < ipAddressesList.size(); ++i)
    {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&  ipAddressesList.at(i).toIPv4Address())
        {
            ip_address = ipAddressesList.at(i).toString();
            qDebug()<<ip_address;  //debug
            //break;
        }
    }
    if (ip_address.isEmpty())
        ip_address = QHostAddress(QHostAddress::LocalHost).toString();
    return ip_address;
}

void MainWindow::on_btnBiosId_clicked()
{
//    DeviceInfo* bios = new DeviceInfo();
//    string uuid = bios->get_bois_id();
//    QString q_str = QString::fromStdString(uuid);
//    ui->lblBiosId->setText(q_str);
    QString res = this->getCmdRes("wmic csproduct get uuid");
    res.remove(0,4);
    ui->lblBiosId->setText(res);
}

QString MainWindow::getCmdRes(QString cmd){
    QProcess p(0);
    p.start("cmd", QStringList()<<"/c"<<cmd);
    p.waitForStarted();
    p.waitForFinished();
    QString strTemp=QString::fromLocal8Bit(p.readAllStandardOutput());
    return strTemp;
}

void MainWindow::on_btnPing_clicked()
{
    ui->txtPing->setPlainText(this->getCmdRes("ping www.baidu.com"));
}
