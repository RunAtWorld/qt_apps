#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_btnGetIp_clicked();

    void on_btnBiosId_clicked();

    void on_btnPing_clicked();

private:
    Ui::MainWindow *ui;
    QString read_ip_address();
    QString getCmdRes(QString cmd);
};

#endif // MAINWINDOW_H
