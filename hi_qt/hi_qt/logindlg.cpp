#include "logindlg.h"
#include "ui_logindlg.h"
#include <QMessageBox>

LoginDlg::LoginDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDlg)
{
    ui->setupUi(this);
    ui->pwdEdit->setEchoMode(QLineEdit::Password);

}

LoginDlg::~LoginDlg()
{
    delete ui;
}


void LoginDlg::on_btnLogin_clicked()
{
    if(ui->usrEdit->text().trimmed() == tr("linux") && ui->pwdEdit->text().trimmed()==tr("123")){
        accept();
    }else{
        QMessageBox::warning(this,tr("警告"),"用户名或密码错误",QMessageBox::Yes);
        ui->usrEdit->clear();
        ui->pwdEdit->clear();
        ui->usrEdit->setFocus();
    }
}
