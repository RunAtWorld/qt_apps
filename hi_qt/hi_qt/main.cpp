#include "mainwindow.h"
#include <QApplication>
#include<QTextCodec>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //设置编码
    QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
    MainWindow w;
    w.show();

    return a.exec();
}
