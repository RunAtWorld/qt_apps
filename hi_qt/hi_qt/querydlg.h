#ifndef QUERYDLG_H
#define QUERYDLG_H

#include <QDialog>

namespace Ui {
class QueryDlg;
}

class QueryDlg : public QDialog
{
    Q_OBJECT

public:
    explicit QueryDlg(QWidget *parent = 0);
    ~QueryDlg();

private:
    Ui::QueryDlg *ui;
};

#endif // QUERYDLG_H
