#include "querydlg.h"
#include "ui_querydlg.h"

QueryDlg::QueryDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QueryDlg)
{
    ui->setupUi(this);
}

QueryDlg::~QueryDlg()
{
    delete ui;
}
