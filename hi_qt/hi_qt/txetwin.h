#ifndef TXETWIN_H
#define TXETWIN_H

#include <QMainWindow>

namespace Ui {
class TxetWin;
}

class TxetWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit TxetWin(QWidget *parent = 0);
    ~TxetWin();

private:
    Ui::TxetWin *ui;
};

#endif // TXETWIN_H
