# 获取设备信息

## 网卡
### 获取网线拔插状态以及扫描网卡信息

QT获取网线拔插状态以及扫描网卡信息（windows）

```
#include <QHostAddress>
#include <QNetworkInterface>
#include <QProcess>
#include <QHostInfo>

void MainWindow::scan_netlink()
{
    QString localHostName=QHostInfo::localHostName();
    ui->display->append(localHostName);//主机名

    QHostInfo info=QHostInfo::fromName(localHostName);
    qDebug()<<"IP Address:"<<info.addresses();//获取当前主机名下ip信息

    foreach(QHostAddress address,info.addresses()){
       if(address.protocol()==QAbstractSocket::IPv4Protocol){
           qDebug()<<"address"<<address.toString();//当前使用的ipv4信息
       }
    }

    QList<QNetworkInterface> list=QNetworkInterface::allInterfaces();//获取所有网卡接口信息到list
    //遍历接口信息
    foreach(QNetworkInterface interface,list){
       qDebug()<<"Device:"<<interface.name();//设备名称
       qDebug()<<"flags"<<interface.flags();
       qDebug()<<"humanReadableName"<<interface.humanReadableName();//接口描述
       qDebug()<<"HardwareAddress:"<<interface.hardwareAddress();//当前接口的MAC信息
       QList<QNetworkAddressEntry> iplist = interface.addressEntries();//获取当前
       QNetworkInterface::InterfaceFlags flags = interface.flags();//获取接口状态信息

       //遍历当前接口下的信息
       foreach(QNetworkAddressEntry s,iplist)
       {
           qDebug()<<"current ip"<<s.ip();
           if(flags.testFlag(QNetworkInterface::IsUp))//查看当前接口是否处于活动状态
           {
               qDebug()<<"is up"<<s.ip().toString();
           }else
           {
               qDebug()<<"is down";
           }
       }
    }
}
```

监测是否插入网线，并使用定时器不断定时监测更新（这样操作可能消耗一些cpu）
```
#include <QNetworkInterface>
bool StatusBarInstance::haveInputNetwork()
{
    QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();
    bool isConnected = false;
    for (int i = 0; i < ifaces.count(); i++)
    {
        QNetworkInterface iface = ifaces.at(i);
        if ( iface.flags().testFlag(QNetworkInterface::IsUp)
             && iface.flags().testFlag(QNetworkInterface::IsRunning)
             && !iface.flags().testFlag(QNetworkInterface::IsLoopBack)
              )
        {
            for (int j=0; j<iface.addressEntries().count(); j++)
            {
                if (isConnected == false)
                    isConnected = true;
            }
        }
    }
    return isConnected;
}
```


使用QProcess和Ping
```
QProcess *cmd = new QProcess;  
#ifdef _TTY_ARMV4_  
QString strArg = "ping -s 1 -c 1 " + b_tmpIpStr;        //linux平台下的格式  
#else  
QString strArg = "ping " + b_tmpIpStr + " -n 1 -w " + QString::number(m_timeoutInt) ;  //windows下的格式  
#endif  
cmd->start(strArg);  
cmd->waitForReadyRead();  
cmd->waitForFinished(); 
//读标准输出
QString retStr=QString::fromLocal8Bit(cmd.readAllStandardOutput());
if (retStr.indexOf("TTL") != -1)  
{  
    qDebug() << m_curIpStr <<"is online!\n";  
}
retStr.clear();
```

使用QTcpSocket连接
```
QTcpSocket socket(0);
socket.abort(); //取消原有连接
socket.connectToHost(m_curIpStr,m_curPort); //建立一个TCP连接
if(socket.waitForConnected(m_timeoutInt))
{
    qDebug()<<m_curIpStr<<"is online\n";
}
socket.close();
```


## 参考
1. [QT获取网线拔插状态以及扫描网卡信息](https://blog.csdn.net/qq_37131073/article/details/109527435?utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.control)