# 常用基础知识

## C++和QT类型互转

### string
String和QString之间的转化

```
QString qstr;
string str;
str = qstr.toStdString();
qstr = QString::fromStdString(str);
```
虽然成功转化了，但是会出现乱码。
```
std::string cstr;
QString qstring;
//从std::string 到QString
qstring = QString(QString::fromLocal8Bit(cstr.c_str()));
//从QString 到 std::string
cstr = string((const char *)qstring.toLocal8Bit());
//不需要从gbk转到utf8
QString value_content = QString::fromStdString(vec[i].content);
```

### int
1.QString转int: 用 `toInt()` 函数

```
QString str("100");
int tmp = str.toInt();
```

或

```
bool ok;
QString str("100");
int tmp = str.toInt(&ok);
```
> 注：ok表示转换是否成功，成功则ok为true，失败则ok为false。

2.int转QString: 用 `QString::number();`
```
int tmp = 100;
QString str = QString::number(tmp);
```

3.GB18030转utf8
```
QBytearry text = QBytearry::fromhex("");
QString::fromLocal8Bit(text.data());
```

4.计算圆形面积并作转换
```
void Dialog::on_radiusLineEdit_textChanged(const QString &arg1)
{
    bool ok;
    QString tempStr;
    QString valueStr=ui->radiusLineEdit->text();
    int valueInt=valueStr.toInt(&ok);
    double area=valueInt*valueInt*PI;//计算圆面积
    ui->areaLabel_2->setText(tempStr.setNum(area));
}
```

## 打印
```
std::string logStr = "";
qDebug() << logStr.c_str();
```

## QT的数据结构

### QList

Java风格: 使用只读迭代器
```c++
QList<int> list;
list<<1<<2<<3<<4<<5;
QListIterator<int> i(list);
for(;i.hasNext();)
    qDebug()<<i.next();
```

Java风格: 使用读写的迭代器
```c++
QList<int> list;
QMutableListIterator<int> i(list);
for(int j=0;j<10;++j)
    i.insert(j);
for(i.toFront();i.hasNext();)
    qDebug()<<i.next();

for(i.toBack();i.hasPrevious();)
{
    if(i.previous()%2==0)
        i.remove();
    else
        i.setValue(i.peekNext()*10);
}

for(i.toFront();i.hasNext();)
    qDebug()<<i.next();
```

STL风格: 迭代器
```c++
QList<int> list;						//初始化一个空的QList<int>列表
for(int j=0;j<10;j++)
    list.insert(list.end(),j);

//初始化一个QList<int>::iterator读写迭代器
QList<int>::iterator i;
for(i=list.begin();i!=list.end();++i)
{
        qDebug()<<(*i);
        *i=(*i)*10;
}

//初始化一个QList<int>:: const_iterator读写迭代器
QList<int>::const_iterator ci;
//在控制台输出列表的所有值
for(ci=list.constBegin();ci!=list.constEnd();++ci)
        qDebug()<<*ci;
```

### QMap

Java风格: QMap 遍历
```c++
QMap<QString,QString> map;					//创建一个QMap栈对象
//向栈对象插入<城市,区号>对
map.insert("beijing","111");
map.insert("shanghai","021");
map.insert("nanjing","025");

QMapIterator<QString,QString> i(map);		//创建一个只读迭代器
for(;i.hasNext();)							//(a)
    qDebug()<<"  "<<i.key()<<"  "<<i.next().value();

QMutableMapIterator<QString,QString> mi(map);
if(mi.findNext("111"))						//QMap不能通过key查询，所以通过值查询
    mi.setValue("010");

QMapIterator<QString,QString> modi(map);
qDebug()<<"  ";
for(;modi.hasNext();)						//再次遍历并输出修改后的结果
    qDebug()<<" "<<modi.key()<<"  "<<modi.next().value();
```

STL风格: QMap 遍历
```c++
QMap<QString,QString> map;
map.insert("beijing","111");
map.insert("shanghai","021");
map.insert("nanjing","025");

QMap<QString,QString>::const_iterator i;
for(i=map.constBegin();i!=map.constEnd();++i)
    qDebug()<<"  "<<i.key()<<"  "<<i.value();

QMap<QString,QString>::iterator mi;
mi=map.find("beijing");
if(mi!=map.end())
    mi.value()="010";				//(a)

QMap<QString,QString>::const_iterator modi;
qDebug()<<"  ";
for(modi=map.constBegin();modi!=map.constEnd();++modi)
    qDebug()<<"  "<<modi.key()<<"  "<<modi.value();
```

赋值：
```
static const QMap<QString, QString> weatherMap {
    {"NA", "NA"},
    {"晴", "0"},
    {"多云", "1"},
    {"阴", "2"},
    {"阵雨", "3"},
    {"雷阵雨", "4"},
};
```
引用，注意是方括号，类似数组：
```
weatherMap[前值] = 后值
```

### QVariant

QVariant类似于C++的共用体
```c++
QVariant v(709);					//(a)
qDebug()<<v.toInt();				//(b)
QVariant w("How are you! ");		//(c)
qDebug()<<w.toString();				//(d)

QMap<QString,QVariant>map;			//(e)
map["int"]=709;						//输入整数型
map["double"]=709.709;				//输入浮点型
map["string"]="How are you! ";		//输入字符串
map["color"]=QColor(255,0,0);		//输入QColor类型的值
//调用相应的转换函数并输出
qDebug()<<map["int"]<< map["int"].toInt();
qDebug()<<map["double"]<< map["double"].toDouble();
qDebug()<<map["string"]<< map["string"].toString();
qDebug()<<map["color"]<< map["color"].value<QColor>();	//(f)

QStringList sl;						//创建一个字符串列表
sl<<"A"<<"B"<<"C"<<"D";
QVariant slv(sl);					//将该列表保存在一个QVariant变量中
if(slv.type()==QVariant::StringList)//(g)
{
    QStringList list=slv.toStringList();
    for(int i=0;i<list.size();++i)
        qDebug()<<list.at(i);		//输出列表内容
}
```

## QT的数学函数

QT的数学函数
```c++
double a=-19.3,b=9.7;
double c=qAbs(a);        				//(a)
double max=qMax(b,c);    				//(b)
int bn=qRound(b);        				//(c)
int cn=qRound(c);

qDebug()<<"a="<<a;
qDebug()<<"b="<<b;
qDebug()<<"c=qAbs(a)= "<<c;
qDebug()<<"qMax(b,c)= "<<max;
qDebug()<<"bn=qRound(b)= "<<bn;
qDebug()<<"cn=qRound(c)= "<<cn;

qSwap(bn,cn);							//(d)
//调用qDebug()函数输出所有的计算结果
qDebug()<<"qSwap(bn,cn):"<<"bn="<<bn<<" cn="<<cn;
```

## String

### QReg
```
QString getBiosId(){
    // cmdRes 的结果为 UUID   \r\r\n814CE1CC-2FF0-11B2-A85C-A893FAE8DB5A  \r\r\n\r\r\n
	QString cmdRes = HwUtils::getCmdRes("wmic csproduct get uuid");
	QRegExp regExp("[\\w\\d-]");
	QRegExp regExp2("[U,u][U,u][I,i][D,d]");
	cmdRes.remove(regExp2);
	QStringList list;
	int pos = 0;
	while ((pos = regExp.indexIn(cmdRes, pos)) != -1)
	{
		list.append(regExp.cap(0));
		pos += regExp.matchedLength();
	}

	QString res = list.join("");
	qDebug() << "bios id:" << res;
	return res;
}

QString HwUtils::getCmdRes(QString cmd){
    QProcess p(0);
    p.start("cmd", QStringList()<<"/c"<<cmd);
    p.waitForStarted();
    p.waitForFinished();
    QString strTemp=QString::fromLocal8Bit(p.readAllStandardOutput());
    return strTemp;
}
```

### QStringList与QString互转
```
QStringList fonts;
fonts << "Arial" << "Helvetica" << "Times" << "Courier";
QString str = fonts.join(","); 


QString str = "name1,path1;name2,path2;name3,path3";
QStringList list1 = str.split(";"); // 注意，如果str是空字符串，list1会增加一个空字符串到列表里，其size=1，我为此吃过苦头～
for(int i=0;i<list1.size();i++)
{
QStringList list2 = list1[i].split(tr(","));
}
```