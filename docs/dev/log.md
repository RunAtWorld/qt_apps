# QT打日志

## 简单日志输出

qInstallMsgHandler实现日志输出

```
#include <QtDebug>
#include <QFile>
#include <QTextStream>

#define _TIME_ qPrintable (QTime::currentTime ().toString ("hh:mm:ss:zzz"))

void Log(QtMsgType type, const char* msg)
{
    QString qstrText;
    switch (type)
    {
    case QtDebugMsg:
        qstrText = QString("%1: %2").arg(_TIME_, msg);
        break;
    case QtWarningMsg:
        qstrText = QString("%1: %2").arg(_TIME_, msg);
        break;
    case QtCriticalMsg:
        qstrText = QString("%1: %2").arg(_TIME_, msg);
        break;
    case QtFatalMsg:
        qstrText = QString("%1: %2").arg(_TIME_, msg);
        exit(0);
    }
    QFile out("log.txt");
    out.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&out);
    ts<<qstrText<<endl;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qInstallMsgHandler(Log);

    qDebug("this is a debug message");
    qWarning("this is a warning message");
    qCritical("this is a critical message");
    qFatal("this is a fatal message");

    return a.exec();
}
```

## 日志写入文件

### 利用ofstream文件写操作
利用ofstream文件写操作，将内存数据写入存储文件。

找到main.cpp：

①、增加头文件引用：

```
#include <QDateTime>
#include <QDebug>
#include <fstream>      // std::ofstream
```

②、增加全局变量：

```
std::ofstream g_OutputDebug;
```

③、定义函数：

```
void outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // 加锁
    static QMutex mutex;
    mutex.lock(); 
    QString text;
    switch(type) {
    case QtDebugMsg:
        text = QString("Debug: ");
        break;
    case QtWarningMsg:
        text = QString("Warning: ");
        break;
    case QtCriticalMsg:
        text = QString("Critical:");
        break;
    case QtFatalMsg:
        text = QString("Fatal: ");
        break;
    default:
        text = QString("Debug: ");
    }

    QString context_info = QString("F:(%1) L:(%2)").arg(QString(context.file)).arg(context.line); // F文件信息L行数
    QString current_date_time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QString current_date = QString("(%1)").arg(current_date_time);
    std::string message = qPrintable(QString("%1 %2 \t%3 \t%4").arg(text).arg(context_info).arg(current_date).arg(msg));
    g_OutputDebug << message << "\r\n"; // std::ofstream
    // 解锁
    mutex.unlock();
}
```

④、增加注册信息及记录到文件中：
```
int main(int argc, char *argv[])
{
    //注册MessageHandler
    qInstallMessageHandler(outputMessage); //注册MessageHandler
    g_OutputDebug.open(qPrintable(QString(QString(QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss").append("-log.txt")))), std::ios::out | std::ios::trunc);

    QApplication a(argc, argv);
    MainWindow w; 
    w.show();
    return a.exec();
}
```

### 利用QFile记录文件

①、增加头文件引用：
```
#include <QDateTime>
#include <QDebug>

#include <QFile>
```
②、定义函数：

```
void outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // 加锁
    static QMutex mutex;
    mutex.lock();
    QByteArray localMsg = msg.toLocal8Bit();
    QString text;
    switch(type) {
    case QtDebugMsg:
        text = QString("Debug: ");
        break;
    case QtWarningMsg:
        text = QString("Warning: ");
        break;
    case QtCriticalMsg:
        text = QString("Critical:");
        break;
    case QtFatalMsg:
        text = QString("Fatal: ");
        break;
    default:
        text = QString("Debug: ");
    }

    // 设置输出信息格式
    QString context_info = QString("F2:(%1) L:(%2)").arg(QString(context.file)).arg(context.line); // F文件L行数
    QString strDateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QString strMessage = QString("%1 %2 \t%3 \t%4").arg(text).arg(context_info).arg(strDateTime).arg(msg);
    // 输出信息至文件中（读写、追加形式）
    QFile file(CODE_SYSTEM_BACKER_PATH+QString(QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss").append("-log.txt")));
    file.open(QIODevice::ReadWrite | QIODevice::Append);
    QTextStream stream(&file);
    stream << strMessage << "\r\n";
    file.flush();
    file.close();
    // 解锁
    mutex.unlock();
}
```

③、增加注册信息：
```
int main(int argc, char *argv[])
{
    //注册MessageHandler
    qInstallMessageHandler(outputMessage); //注册MessageHandler

    QApplication a(argc, argv);
    MainWindow w; 
    w.show();
    return a.exec();
}
```

这样，我们就得到了输出日志了。（注：Release版本稍许有些不同，需要引用DEFINES += QT_MESSAGELOGCONTEXT到Pro中）


## 参考

1. [Qt输出打印信息的日志到文件（两种方式）](https://blog.csdn.net/u014597198/article/details/78675246)
