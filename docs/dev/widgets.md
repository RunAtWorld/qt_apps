# QT控件

## QPushButton

```c++
#include <qapplication.h>
#include <qpushbutton.h>
#include <qfont.h>
MyWidget::MyWidget(QWidget *parent): QWidget(parent)
{
    setMinimumSize( 200, 120 );
    setMaximumSize( 200, 120 );
    QPushButton *quit = new QPushButton( "Quit", this);
    quit->setGeometry( 62, 40, 75, 30 );
    quit->setFont( QFont( "Times", 18, QFont::Bold ) );
    connect( quit, SIGNAL(clicked()), qApp, SLOT(quit()) );
}

MyWidget::~MyWidget()
{
}
```

## QListWidget

### 常用函数讲解

（1）addItem函数: 添加一项或多项内容。
```
void addItem(const QString &label);
void addItem(QListWidgetItem *item);
void addItems(const QStringList &labels);
```

（2）insertItem函数: 插入新项到列表框。
```
void insertItem(int row, QListWidgetItem *item);
void insertItem(int row, const QString &label);
void insertItems(int row, const QStringList &labels);
```

（3）count()函数： 包含隐藏的列表项在内的列表项总数。
```
int count() const;
```

（4）currentRow函数: 返回当前选择的项的序号。
```
int currentRow () const;
```

（5）takeItem(int) 函数: 删除第row行的项。
```
QListWidgetItem* takeItem(int row);
```

（6）setCurrentRow(int) 函数: 设置当前选中第几行
```
QListWidgetItem* takeItem(int row);
```

### 信号

1. currentTextChanged(QString): 当前文字发生变化
2. void QListWidget::currentTextChanged(const QString &currentText): 当前项发生变化

### 例子

```
#include <QtGui/QApplication>
#include <QWidget>
#include <QLabel>
#include <QListWidget>
#include <QHBoxLayout>
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QLabel *label = new QLabel;
    label->setFixedWidth(100);
    QListWidget *listWidget = new QListWidget;
    listWidget->addItem(new QListWidgetItem(QIcon(QObject::tr("images/china.png")), QObject::tr("China")));
    listWidget->addItem(new QListWidgetItem(QIcon(QObject::tr("images/hk.png")), QObject::tr("Hong Kong")));
    listWidget->addItem(new QListWidgetItem(QIcon(QObject::tr("images/macau.png")), QObject::tr("Macau")));
    
    QHBoxLayout *mainlayout = new QHBoxLayout;
    mainlayout->addWidget(listWidget);
    mainlayout->addWidget(label);
    QObject::connect(listWidget, SIGNAL(currentTextChanged(QString)), label, SLOT(setText(QString)));
    
    QWidget *widget = new QWidget;
    widget->setLayout(mainlayout);
    widget->setWindowTitle(QObject::tr("QListWidget Demo"));
    widget->show();

    return a.exec();
}
```

效果

![](pics/2012101619414018.png)

## QTreeWidget

### 例子

```
#include <QtGui/QApplication>
#include <QtCore/QTextCodec>
#include <QTreeWidget>
#include <QWidget>
#include <QHBoxLayout>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("gb18030"));
    QTreeWidget *tree = new QTreeWidget;
    tree->setColumnCount(2);
    QStringList headers;
    headers << QObject::tr("科目") <<QObject::tr("分数");
    tree->setHeaderLabels(headers);

    QStringList zhangsan;
    zhangsan << QObject::tr("张三");
    QTreeWidgetItem *zhangsanroot = new QTreeWidgetItem(tree, zhangsan);
    QStringList zhangsanChinese;
    zhangsanChinese << QObject::tr("语文") << QObject::tr("80");
    QTreeWidgetItem *leaf1 = new QTreeWidgetItem(zhangsanroot, zhangsanChinese);
    zhangsanroot->addChild(leaf1);
    QStringList zhangsanMath;
    zhangsanMath << QObject::tr("数学") << QObject::tr("90");
    QTreeWidgetItem *leaf2 = new QTreeWidgetItem(zhangsanroot, zhangsanMath);
    zhangsanroot->addChild(leaf2);

    QStringList lisi;
    lisi << QObject::tr("李四");
    QTreeWidgetItem *lisiroot = new QTreeWidgetItem(tree, lisi);
    QStringList lisiChinese;
    lisiChinese << QObject::tr("语文") << QObject::tr("90");
    leaf1 = new QTreeWidgetItem(lisiroot, lisiChinese);
    lisiroot->addChild(leaf1);
    QStringList lisiMath;
    lisiMath << QObject::tr("数学") << QObject::tr("100");
    leaf2 = new QTreeWidgetItem(lisiroot, lisiMath);
    lisiroot->addChild(leaf2);

    tree->addTopLevelItem(zhangsanroot);
    tree->addTopLevelItem(lisiroot);

    QHBoxLayout *mainlayout = new QHBoxLayout;
    mainlayout->addWidget(tree);
    QWidget *widget = new QWidget;
    widget->setLayout(mainlayout);
    widget->setWindowTitle(QObject::tr("QTreeWidget Demo"));
    widget->show();
    return a.exec();
}
```

效果

![](pics/2012101619521098.png)

## QTableWidget

```
#include <QtGui/QApplication>
#include <QTextCodec>
#include <QTableWidget>
#include <QWidget>
#include <QVBoxLayout>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("gb18030"));
    QTableWidget *table = new QTableWidget;
    table->setRowCount(3);
    table->setColumnCount(3);

    QStringList horizontalHeader;
    horizontalHeader<<QObject::tr("年龄")<<QObject::tr("身高")<<QObject::tr("体重");
    QStringList verticalHeader;
    verticalHeader<<QObject::tr("张三")<<QObject::tr("李四")<<QObject::tr("王五");

    table->setHorizontalHeaderLabels(horizontalHeader);
    table->setVerticalHeaderLabels(verticalHeader);

    table->setItem(0, 0, new QTableWidgetItem(QString("23")));
    table->setItem(0, 1, new QTableWidgetItem(QString("177cm")));
    table->setItem(0, 2, new QTableWidgetItem(QString("60kg")));
    table->setItem(1, 0, new QTableWidgetItem(QString("25")));
    table->setItem(1, 1, new QTableWidgetItem(QString("180cm")));
    table->setItem(1, 2, new QTableWidgetItem(QString("70kg")));
    table->setItem(2, 0, new QTableWidgetItem(QString("29")));
    table->setItem(2, 1, new QTableWidgetItem(QString("172cm")));
    table->setItem(2, 2, new QTableWidgetItem(QString("65kg")));

    QWidget *widget = new QWidget;
    QVBoxLayout *mainlayout = new QVBoxLayout;
    mainlayout->addWidget(table);

    widget->setLayout(mainlayout);
    widget->resize(400, 200);
    widget->setWindowTitle(QObject::tr("QTableWidget Demo"));
    widget->show();

    return a.exec();
}
```

效果

![](pics/2012101719220422.png)

## 参考
1. https://blog.csdn.net/naibozhuan3744/article/details/79403147
2. https://www.cnblogs.com/venow/archive/2012/10/16/2726576.html