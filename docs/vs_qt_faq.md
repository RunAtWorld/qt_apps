# VS开发qt问题FAQ

## 在ui文件中修改的控件，在vs里面找不到
在ui文件中修改的控件，在vs里面找不到。解决如下: 

1. 保存Ui文件: 在拖拽控件之后，Ctrl+S

2. 选中项目，批量清理，再批量编译

![image-20210823211115734](imgs/image-20210823211115734.png)

![image-20210823211224774](imgs/image-20210823211224774.png)

3. 重新编译ui文件

![image-20210823211315213](imgs/image-20210823211315213.png)

4. 右键项目，重新扫描解决方案

![image-20210823211403835](imgs/image-20210823211403835.png)



## There's no Qt version assigned

 ![image-20210907204730157](imgs/image-20210907204730157.png)

然后选择自己的qt版本选项

## 参考

1. https://www.cnblogs.com/wanghongyang/p/14967726.html