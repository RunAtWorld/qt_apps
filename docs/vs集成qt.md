# qt 和 visual studio集成开发

## 简述

之前介绍过 Qt5.x 的环境搭建，5.7 开始支持 VS2015，为了使用新的开发环境（典型的强迫症），不得不再次进行 Qt5.7 + VS2015 的环境搭建。

除了之前介绍的搭建细节之外，其实中间有很多需要注意的部分。下面，主要分享搭建过程以及其中需要注意的一些事项。

## 安装 VS2015

进入 [VS2015 官方下载页面](https://www.visualstudio.com/downloads/)，下载 VS 社区版 - Visual Studio Community。

![这里写图片描述](imgs/20161230121024778.png)

下载完成之后，直接进行安装，傻瓜式 - 下一步。

**注意：**安装 VS2015 时，切记勾选“Visual C++”选项。

## 安装 Qt5.7

进入 [Qt 官方下载页面](http://download.qt.io/archive/qt/)，下载 Qt5.7，我下载的是：qt-opensource-windows-x86-msvc2015_64-5.7.1.exe。

![这里写图片描述](imgs/2.png)

下载完成之后，直接进行安装，傻瓜式 - 下一步。

**注意：**这时还不算完整，因为并非所得的配置都准备就绪，下面一起来看如何配置编译器与调试器。

为了能够同时使用 Qt Creator 和 VS2015，必须分别对他们进行一系列的配置。

## 配置 Qt Creator

### 配置编译器

如果没有检测到编译器，大多数情况下，是因为安装 VS2015 时候没有勾选“Visual C++”选项。

**注意：**检测不出来没关系，千万不要卸载重装（遇到问题就卸载重装、重启机器的人，只能说：真是厉害了，Word 哥！），只需要修改下即可。

进入控制面板 -> 程序和功能，找到 Microsoft Visual Studio Community 2015 Updates，右键选择“更改”。

![这里写图片描述](imgs/3.png)

勾选“Visual C++”选项，再次安装，搞定！

![这里写图片描述](imgs/4.png)

这时，打开 Qt Creator，进入编译器部分，可以看到 Qt 已经自动检测出来了，不需要手动配置。

![这里写图片描述](imgs/5.png)

可以看到，自动检测出来的构建套件前面显示的警告符号，调试器部分显示“None”,这说明还没有配置调试器！

![这里写图片描述](imgs/6.png)

### 配置调试器

调试器默认情况下是没有的，必须手动下载 [windbg](https://developer.microsoft.com/zh-cn/windows/hardware/download-windbg) 。

在安装过程中，需要勾选“Debugging Tools for Windows”。

![这里写图片描述](imgs/7.png)

安装完成之后，打开 Qt Creator（如果已经打开，请先关闭，再重新打开）。这时，可以看到 Qt 已经自动检测出调试器了，很简单吧！

![这里写图片描述](imgs/8.png)

在“构建套件（Kit）”中选择自动检测出来的调试器即可。

![这里写图片描述](imgs/9.png)

### Hello World

迫不及待了吧，那就赶紧写个 Hello World 调试一下喽！

![这里写图片描述](imgs/10.png)

## VS2015 配置

### 安装插件

VS2015 之前集成 Qt 都可以用 Qt Add-In，但在 Visual Studio 2015 中，Qt 插件不可用了，但是提供了一个扩展（工具 -> 扩展和更新…）：

![这里写图片描述](imgs/11.png)

选择：联机，搜索关键字“Qt”，就会出现相关插件：

![这里写图片描述](imgs/12.png)

可以看到两个很类似的插件：

- Qt Visual Studio Tools (2015)：创建者为“The Qt Company Ltd”，右下角的截图可以看出这正是我们想要的插件。
- QtPackage：创建者为“Konstantin Stukov”，右下角的截图和上面的差不多。这个包也可以在 [QtPackage](https://marketplace.visualstudio.com/items?itemName=havendv.QtPackage) 下载。

选择哪个好呢？不清楚的时候，永远确定一点，选择最信任的。。。那必然就是官方提供的 Qt Visual Studio Tools (2015) 了。

![这里写图片描述](imgs/13.png)

安装后，重新启动 VS2015，在菜单栏上就会看到“Qt VS Tools”菜单项：

![这里写图片描述](imgs/14.png)

### 配置 Qt 5.7

选择：Qt VS Tools -> Qt Options，配置 Qt 5.7。点击“Add”按钮，Path 选择 D:\Qt\Qt5.7.1\5.7\msvc2015_64，然后点击“Ok”进行保存。

注意：如果没有配置QT，在新建Qt项目时会出现“Unable to find a Qt build!”错误

![这里写图片描述](imgs/15.png)

### Hello World

配置好环境就赶紧测试一下吧，写个小程序，最熟悉的 Hello World。

选择：文件 -> 新建 -> 项目，然后选择：模板 -> Visual C++ -> Qt -> Qt GUI Application。

![这里写图片描述](imgs/16.png)

输入项目名称后，点击“确定”按钮。

![这里写图片描述](imgs/17.png)

这时，会自动生成相应的代码，只需要简单地编译、运行即可。

## 参考

1. [VS+Qt的环境搭建](https://blog.csdn.net/weixin_43277501/article/details/89850880)
2. [Qt环境搭建（Visual Studio）](http://blog.csdn.net/liang19890820/article/details/49874033)
3. [Qt环境搭建（Qt Creator）](http://blog.csdn.net/liang19890820/article/details/49894691)