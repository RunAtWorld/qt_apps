# Qt 官网

- [Qt 官网](https://www.qt.io/)
- [Qt 下载](http://www.qt.io/download)
- [Qt 所有下载](http://download.qt.io/archive/qt)
- [Qt 官方发布下载](http://download.qt.io/official_releases/qt)
- [Open Source 下载](http://www.qt.io/download-open-source/#section-2)
- [Qt WiKi](https://wiki.qt.io/Main_Page)

# 编码风格

- Qt Coding Style
  - [low-level](http://wiki.qt.io/Qt_Coding_Style)
  - [higher-level](http://wiki.qt.io/Coding_Conventions)
- [Google 开源项目风格指南](http://zh-google-styleguide.readthedocs.io/en/latest)
  里面包含五份（C++ 、Objective-C、Python 、JSON、Shell ）中文版的风格指南。
- [C 编码风格指南](http://www.quinapalus.com/coding.html)
- [C++ 编码标准](http://www.possibility.com/Cpp/CppCodingStandard.html)

# GitHub & Third-Party

- [QtProject](https://github.com/qtproject)
  各种强大的 Qt 项目及丰富的 Qt 库。

- Awesome 系列：
  - [awesome-cpp](https://github.com/fffaraz/awesome-cpp)
  - [awesome-qt](https://insideqt.github.io/awesome-qt)
    一系列强大的 C/C++ 框架、库、资源和其它好东西。
- [inqlude](https://inqlude.org/)
  Qt 库存档 - 为 Qt 应用程序开发人员提供了所有现有的库。
- [free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN)
  免费的计算机编程类中文书籍

# 社区论坛

国外论坛：

- [Qt-Centre](http://www.qtcentre.org/)
  一个非常全面的外国网站，有 forum、wiki、docs、blogs 等。
- [Qt-forum](http://www.qtforum.org/)
  比较有名的国际 Qt 编程论坛

- [Developpez](https://qt.developpez.com/)
  一个法语社区，致力于信息技术的发展。该网站向读者免费提供资源和服务：博客、论坛、教程、在线课程、源代码等。

- [Qt-Apps](http://qt-apps.org/)
  可以找到很多免费的 Qt 应用，获得源码来学习、研究，使用时请遵守相关开源协议。
- [KDE-Apps](http://kde-apps.org/)
  用过 Linux/KDE 的应该比较熟悉，里面有许多 KDE-Desktop 相关的应用。

- [Qt Software](http://www.qtsoftware.com/)
  提供越来越多的第三方商业软件和开源软件的 Qt 用户社区。
- [QUIt Coding](http://quitcoding.com/)
  一群享受前沿技术开发的人才，Qt 官方大使项目的成员。

国内论坛：

- [CSDN Qt 论坛](http://bbs.csdn.net/forums/Qt)

  作为中国最大的IT社区和服务平台，CSDN 也在持续的关注 Qt 的发展，Qt 技术社区也已经上线很久了，可以在里面进行知识传播 - 提问、分享自己的一些学习心得、资料等。

- [QTCN 开发网](http://www.qtcn.org/)
  国内最早、最活跃的 Qt 中文社区，内容丰富、覆盖面广、在线人数众多，上面有很多热心、无私的 Qt 爱好者，他们会帮助初学者尽快的入门。

- [Qter 开源社区](http://www.qter.org/)
  致力于 Qt 普及工作！里面富含 Qter 们开发的实用开源项目和作品，以及一系列优秀的原创教程 - 图文并茂、简单易学，力争帮助每一位初学者快速入门。

# 博客

- [一去丶二三里](https://waleon.blog.csdn.net/)
  纯正开源之美，有趣、好玩、靠谱。。。
- [齐亮](http://qihome.org/)
  修身-“齐家”-治天下。
- [豆子](http://www.devbean.net/)
  Colorful Technologies… 《Qt 学习之路》系列很不错。
- [1+1=10](http://blog.csdn.net/dbzhang800)
  简简单单
- [foruok](http://blog.csdn.net/foruok)
  关注程序员的职业规划，愿更多人找到适合自己的修行路。
- [Planet Qt](http://planet.qt.io/)
  Qt 相关博客的汇集者，无论作者是谁，它包含的意见是各自作者的。

# 书籍

- [QmlBook](http://qmlbook.github.io/)
  关于 Qt5 的 QML 书籍
- 书名：《C++ Primer plus》
  作者：Stephen Prata
  介绍：从入门到精通必读经典教程。它被誉为“开发人员学习 C++ 的教程，没有之一”！

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTYyNjI2MjUy?x-oss-process=image/format,png)

- 书名：《C++ Primer》
  作者：Stanley B. Lippman、Josee Lajoie、Barbara E. Moo
  介绍：久负盛名的经典教程，系统全面地介绍了 C++，可以看成是学习 C++ 的百科全书，C++ 程序猿必备。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI1MTYyNTU5OTY3?x-oss-process=image/format,png)

- 书名：《C++ GUI Qt 4 编程》
  作者：Jasmin Blanchette、Mark Summerfield
  介绍：Trolltech 的 Qt 培训教材，生动、全面、深刻地阐明了 Qt 程序的设计理念，轻松创建跨平台的解决方案。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTYyODQwMTM0?x-oss-process=image/format,png)

- 书名：《Qt 高级编程》
  作者：Mark Summerfield
  介绍：阐述 Qt 高级编程技术的书籍。以工程实践为主旨，是对 Qt 现有的 700 多个类和上百万字参考文档中部分关键技术深入、全面的讲解和探讨。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTYzMTMzODc5?x-oss-process=image/format,png)

- 书名：《Python Qt GUI 快速编程》
  作者：Mark Summerfield
  介绍：讲述如何利用 Python 和 Qt 开发 GUI 应用程序的原理、方法和关键技术。结构合理，内容详实，适合用作对Python、Qt 和 PyQt 编程感兴趣的童鞋。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYxMDEyMTA1NTU2OTUx?x-oss-process=image/format,png)

- 书名：《C++ Qt 设计模式》
  作者： Alan Ezust
  介绍：利用跨平台开源软件开发框架 Qt 阐释了 C++ 和设计模式中的主要思想，既复习了设计模式，又学了 C++/Qt，对于使用其它框架也是一个非常有用的参考。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI1MTYwNDI3MDAw?x-oss-process=image/format,png)

- 书名：《Qt5 开发实战》
  作者：金大zhen、张红艳 译
  介绍：在全面阐述 Qt 基本功能的基础上，对新增的功能和服务进行了重点介绍。同时运用大量示例，集中讲解了应用程序的开发方法、技巧和必需的 API。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTgwOTA5MzA5?x-oss-process=image/format,png)

- 书名：《Qt5 开发及实例》
  作者：陆文周
  介绍：以 Qt5.4 为平台，循序渐进，在介绍开发环境的基础上，系统介绍 Qt5 应用程序的开发技术，通过实例介绍和讲解内容，将知识和能力融为一体。一般能够在比较短的时间内掌握 Qt5 应用技术。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTYzNTIzNDEy?x-oss-process=image/format,png)

- 书名：《Qt on Android 核心编程》
  作者：安晓辉
  介绍：基于 Qt 5.2，详细讲述如何在移动平台 Android 上使用 Qt 框架进行开发。无论是专注于传统的桌面软件开发，还是希望尝试使用 Qt 在 Android 平台开发，都可以从中获得重要的知识与实例。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTYzNzI2NDc1?x-oss-process=image/format,png)

- 书名：《Qt Quick 核心编程》
  作者：安晓辉
  介绍：着力于 QML 语言基础、事件、Qt Quick 基本元素，辅以简要的 ECMAScript（JavaScript）语言介绍，能够快速熟悉 Qt Quick 的基本知识和开发过程，详尽入微。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTY0MDE2NTcw?x-oss-process=image/format,png)

- 书名：《Qt Creator快速入门 》
  作者：霍亚飞
  介绍：基于 Qt Creator 编写，全面涉及 Qt Quick；植根于 Qt 网络博客教程，可无限更新；对每个知识点详尽讲解，并设计了示例程序。

  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI1MTcwODAzOTAy?x-oss-process=image/format,png)

- 书名：《Qt5 编程入门》
  作者：霍亚飞、devbean
  介绍：基于 Qt5.3 编写，全面涉及 Qt Quick；植根于 Qt 网络博客教程，可无限更新；对每个知识点详尽讲解，并设计了示例程序。
  
  ![这里写图片描述](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTYwNjI0MTY0OTA1NDE0?x-oss-process=image/format,png)

# 最后的话

如果你是新手，建议从 Qt5 学起；如果你要买 Qt4 相关的书，只建议这两本 - 《C++ GUI Qt 4 编程》和《Qt 高级编程》 。