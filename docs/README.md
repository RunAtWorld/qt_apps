# QT学习成长

## QT资料

### 学习资料
1. [Qt官网](https://www.qt.io/zh-cn/)
2. [Qt 快速入门系列教程](http://shouce.jb51.net/qt-beginning/)
3. [Qt 编程指南](https://qtguide.ustclug.org/)
4. [Qt初识](http://c.biancheng.net/view/1792.html)

### 视频教程

1. [QT从入门到实战视频教程](http://yun.itheima.com/course/539.html?stt)

## 开发之路

1. Qt入门
   1. Qt概述
   2. 第一个Qt小程序
   3. 信号和槽机制
2. Qt界面编写
   1. Qt窗体
   2. 常用控件
   3. 布局管理
3. Qt高级
   1. 消息机制和事件
   2. 绘图设备
   3. 文件系统
4. 实例演练
   1. 项目配置
   2. 设置各个场景
   3. 游戏类设计
   4. 特性设计
   5. 音效和优化

## Qt官方

1. [qt官方下载](https://download.qt.io/archive/qt/)
2. [qt 官方资源库](https://download.qt.io/)

